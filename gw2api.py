# gw2api Lampy - Python wrapper for Guild Wars 2 official API.
# Developer: Farshid Hassani Bijarbooneh
# Version 1.3 - 29-07-2015
# Endpoints:
# https://wiki.guildwars2.com/wiki/API:2

import urllib2
import urllib
from concurrent import futures
import concurrent

try:
    import json
except ImportError:
    import simplejson as json

class GW2API:
    V1 = 'v1'
    V2 = 'v2'
    BaseUrl = 'https://api.guildwars2.com/'


def load_url(url, timeout):
    conn = urllib2.urlopen(url, timeout=timeout)
    return conn.read()


def _request(gw2api_version, json_location, **args):
    # Makes a request on the Guild Wars 2 API.
    url = GW2API.BaseUrl + gw2api_version + '/' + json_location + '?' + '&'.join(
        str(argument) + '=' + str(value) for argument, value in args.items() if argument != 'authorization')
    print url
    req = urllib2.Request(url)
    if 'authorization' in args:
        req.add_header('authorization', args['authorization'])
    return req


def fetch_requests(requests, max_parallel_requests, timeout):
    # slightly improved version of http://stackoverflow.com/questions/16181121/python-very-simple-multithreading-parallel-url-fetching-without-queue
    with futures.ThreadPoolExecutor(max_workers=max_parallel_requests) as executor:
        # Start the load operations and mark each future with its URL and an index in the call order
        if not isinstance(requests, list):
            requests = [requests]
        data = {}
        req_idx = [i for i in range(len(requests))]
        future_to_url = {executor.submit(load_url, req, timeout): (req, idx) for req, idx in zip(requests, req_idx)}

        for future in concurrent.futures.as_completed(future_to_url):
            (req, idx) = future_to_url[future]
            try:
                data[idx] = future.result()
            except Exception as exc:
                print '%r generated an exception: %s' % (req, exc)
                # else:
                # print '"%s" fetched data %ss' % (req, data[idx])
        return data


def request_account(authKey):
    # Creates a request to get account information
    return _request(GW2API.V2, 'account', authorization=authKey)


def request_characters(authKey):
    # Creates a request to get a list of all characters names
    return _request(GW2API.V2, 'characters', authorization=authKey)


def request_character_details(ids, authKey):
    # Creates a request to get the details of a given character name
    return _request(GW2API.V2, 'characters', ids=urllib.quote_plus(','.join(ids)), authorization=authKey)


def request_transactions_current_buys(authKey):
    # Creates a request to get the details of the current buy orders
    return _request(GW2API.V2, 'commerce/transactions/current/buys', authorization=authKey)


def request_transactions_current_sells(authKey):
    # Creates a request to get the details of the current sell orders
    return _request(GW2API.V2, 'commerce/transactions/current/sells', authorization=authKey)


def request_transactions_history_buys(authKey):
    # Creates a request to get the details of the history of buy orders
    return _request(GW2API.V2, 'commerce/transactions/history/buys', authorization=authKey)


def request_transactions_history_sells(authKey):
    # Creates a request to get the details of the history of sell orders
    return _request(GW2API.V2, 'commerce/transactions/history/sells', authorization=authKey)


def request_tokeninfo(authKey):
    # Creates a request to get a list of all characters names
    return _request(GW2API.V2, 'tokeninfo', authorization=authKey)


def request_items():
    # Creates a request to get the list of all item ids.
    return _request(GW2API.V2, 'items')


def request_item_details(ids, lang='en'):
    # Creates a request to get the details of a list of items.
    return _request(GW2API.V2, 'items', ids=','.join(str(id) for id in ids), lang=lang)


def request_recipes():
    # Creates a request to get a list of all recipes.
    return _request(GW2API.V2, 'recipes')


def request_recipe_details(ids, lang='en'):
    # Creates a request to get the recipe details of a list of items.
    return _request(GW2API.V2, 'recipes', ids=','.join(str(id) for id in ids), lang=lang)


def request_recipe_search(search_type, id):
    # Creates a request to search for a recipe using input for items as an ingredient
    # and with output for recipes that craft that item.
    s = search_type
    if search_type == 'input':
        return _request(GW2API.V2, 'recipes/search', input=id)
    elif search_type == 'output':
        return _request(GW2API.V2, 'recipes/search', output=id)
    else:
        print "search type mismatch!"


def request_skins():
    # Creates a request to get a list of all skin ids.
    return _request(GW2API.V2, 'skins')


def request_skin_details(ids, lang='en'):
    # Creates a request to get the skin details for all items listed in ids.
    return _request(GW2API.V2, 'skins', ids=','.join(str(id) for id in ids), lang=lang)


def request_continents(**kwargs):
    # Creates a request to get the list of continents and details depending of the given parameters
    # parameters: id='', floor='', regions='', maps='', options='', lang='en'
    if 'lang' not in kwargs:
        kwargs['lang'] = 'en'
    if 'id' not in kwargs:
        return _request(GW2API.V2, 'continents', lang=kwargs['lang'])
    elif 'floor' not in  kwargs:
        return _request(GW2API.V2, 'continents' + '/' + str(kwargs['id']), lang=kwargs['lang'])
    elif 'regions' not in  kwargs:
        return _request(GW2API.V2, 'continents' + '/' + str(kwargs['id']) + '/floors/' + str(kwargs['floor']), lang=kwargs['lang'])
    elif 'maps' not in kwargs:
        return _request(GW2API.V2, 'continents' + '/' + str(kwargs['id']) + '/floors/' + str(kwargs['floor']) + '/regions/' 
                        + str(kwargs['regions']), lang=kwargs['lang'])
    elif 'options' not in kwargs:
        return _request(GW2API.V2, 'continents' + '/' + str(kwargs['id']) + '/floors/' + str(kwargs['floor']) + '/regions/' 
                        + str(kwargs['regions']) + '/maps/' + str(kwargs['maps']), lang=kwargs['lang'])
    else:
        return _request(GW2API.V2, 'continents' + '/' + str(kwargs['id']) + '/floors/' + str(kwargs['floor']) + '/regions/' 
                        + str(kwargs['regions']) + '/maps/' + str(kwargs['maps']) + '/' + kwargs['options'], lang=kwargs['lang'])


def request_maps(lang='en', **kwargs):
    """ Creates a request to get the list of all maps ids or details of the given maps ids.
    see subprocess.call for some documentations.
    :param lang: Language of data
    :param kwargs: accepts keyword 'ids' as a list of all map ids.
    """
    if 'ids' not in kwargs:
        return _request(GW2API.V2, 'maps', lang=lang)
    else:
        return _request(GW2API.V2, 'maps', ids=','.join(str(id) for id in kwargs['ids']), lang=lang)


def request_listings(*ids):
    # Creates a request to get the list of all items on TP or listing details of the given item ids.
    if len(ids) < 1:
        return _request(GW2API.V2, 'commerce/listings')
    else:
        return _request(GW2API.V2, 'commerce/listings', ids=','.join(str(id) for id in ids[0]))


def request_prices(*ids):
    # Creates a request to get the list of all items on TP or price details of the given item ids.
    if len(ids) < 1:
        return _request(GW2API.V2, 'commerce/prices')
    else:
        return _request(GW2API.V2, 'commerce/prices', ids=','.join(str(id) for id in ids[0]))


def request_exchange(exchange_type, quantity):
    # Creates a request to get the exchange rate for the given quantity of coins or gems.
    if exchange_type == 'coins_to_gems':
        return _request(GW2API.V2, 'commerce/exchange/coins', quantity=quantity)
    elif exchange_type == 'gems_to_coins':
        return _request(GW2API.V2, 'commerce/exchange/gems', quantity=quantity)
    else:
        print "Exchange type mismatch!"


def request_build():
    # Creates a request to get the build id of the game.
    return _request(GW2API.V2, 'build')


def request_colors(*ids):
    # Creates a request to get the list of all colors or details of the given color ids.
    if len(ids) < 1:
        return _request(GW2API.V2, 'colors')
    else:
        return _request(GW2API.V2, 'colors', ids=','.join(str(id) for id in ids[0]))


def request_files(*ids):
    # Creates a request to get the list of all files id or details of the given file ids.
    if len(ids) < 1:
        return _request(GW2API.V2, 'files')
    else:
        return _request(GW2API.V2, 'files', ids=','.join(str(id) for id in ids[0]))


def request_quaggans(*ids):
    # Creates a request to get the list of all quaggans or details of the given quaggans ids.
    if len(ids) < 1:
        return _request(GW2API.V2, 'quaggans')
    else:
        return _request(GW2API.V2, 'quaggans', ids=','.join(str(id) for id in ids[0]))


def request_worlds(*ids):
    # Creates a request to get the list of all worlds or details of the given worlds ids.
    if len(ids) < 1:
        return _request(GW2API.V2, 'worlds')
    else:
        return _request(GW2API.V2, 'worlds', ids=','.join(str(id) for id in ids[0]))

################# Endpoints for the API Version 1 #################
def request_matches():
    # Creates a request to get the list of all WvW matches.
    return _request(GW2API.V1, 'wvw/matches.json')

def request_match_details(id):
    # Creates a request to get the list of the details of the given match id.
    return _request(GW2API.V1, 'wvw/match_details.json', match_id=id )

def request_objective_names(lang='en'):
    # Creates a request to get the list of the details of the given match id.
    return _request(GW2API.V1, 'wvw/objective_names.json', lang=lang )

def request_event_names(lang='en'):
    # Creates a request to get the list of the localised event names.
    return _request(GW2API.V1, 'event_names.json', lang=lang )

def request_event_details(lang='en', **kwargs):
    # Creates a request to get the list of the details of the localised event ids.
    if 'event_id' not in kwargs:
        return _request(GW2API.V1, 'event_details.json', lang=lang)
    else:
        return _request(GW2API.V1, 'event_details.json', event_id=kwargs['event_id'], lang=lang )

def request_guild_details(**kwargs):
    # Creates a request to get the list of the details of the given guild id or guild name.
    if 'guild_id' in kwargs:
        return _request(GW2API.V1, 'guild_details.json', guild_id=kwargs['guild_id'])
    elif 'guild_name' in kwargs:
        return _request(GW2API.V1, 'guild_details.json', guild_name=urllib.quote_plus(kwargs['guild_name']) )
    else:
        print "Please include either guild_id or guild_name parameters!"
