__author__ = 'farshidhassanibijarbooneh'
import gw2api
import pprint, json

# Timeout of receiving response from the requests
TIMEOUT = 60
APIKEY = 'Place your API key here!!!'

def test_account():
    authKey = 'Bearer ' + APIKEY
    req = gw2api.request_account(authKey)
    res = gw2api.fetch_requests(req, 1, TIMEOUT)
    print res[0]


def test_characters():
    authKey = 'Bearer ' + APIKEY
    req = gw2api.request_characters(authKey)
    res = gw2api.fetch_requests(req, 1, TIMEOUT)
    print res[0]


def test_character_details():
    authKey = 'Bearer ' + APIKEY
    char_names = ['The Lampoo', 'Lampoo']
    req = gw2api.request_character_details(char_names, authKey)
    res = gw2api.fetch_requests(req, 1, TIMEOUT)
    print json.dumps(json.loads(res[0]), indent=4)


def test_transactions_current_buys():
    authKey = 'Bearer ' + APIKEY
    req = gw2api.request_transactions_current_buys(authKey)
    res = gw2api.fetch_requests(req, 1, TIMEOUT)
    print res


def test_transactions_current_sells():
    authKey = 'Bearer ' + APIKEY
    req = gw2api.request_transactions_current_sells(authKey)
    res = gw2api.fetch_requests(req, 1, TIMEOUT)
    print res


def test_transactions_history_buys():
    authKey = 'Bearer ' + APIKEY
    req = gw2api.request_transactions_history_buys(authKey)
    res = gw2api.fetch_requests(req, 1, TIMEOUT)
    print res


def test_transactions_history_sells():
    authKey = 'Bearer ' + APIKEY
    req = gw2api.request_transactions_history_sells(authKey)
    res = gw2api.fetch_requests(req, 1, TIMEOUT)
    print res


def test_tokeninfo():
    authKey = 'Bearer ' + APIKEY
    req = gw2api.request_tokeninfo(authKey)
    res = gw2api.fetch_requests(req, 1, TIMEOUT)
    print res


def test_items():
    req = gw2api.request_items()
    res = gw2api.fetch_requests(req, 1, TIMEOUT)
    print res


def test_item_details():
    oneItem = [71]
    moreItems = [87, 88, 89, 90, 91, 92, 93]
    req = []
    req.append(gw2api.request_item_details(oneItem))
    req.append(gw2api.request_item_details(moreItems))
    res = gw2api.fetch_requests(req, 2, TIMEOUT)
    print res


def test_recipe():
    req = gw2api.request_recipes()
    res = gw2api.fetch_requests(req, 1, TIMEOUT)
    print res


def test_recipe_details():
    oneItem = [27]
    moreItems = [27, 28, 29, 30, 31]
    req = []
    req.append(gw2api.request_recipe_details(oneItem))
    req.append(gw2api.request_recipe_details(moreItems))
    res = gw2api.fetch_requests(req, 2, TIMEOUT)
    print res


def test_recipe_search():
    search_type = 'input'
    itemId = 46731
    req = []
    req.append(gw2api.request_recipe_search(search_type, itemId))

    search_type = 'output'
    itemId = 50065
    req.append(gw2api.request_recipe_search(search_type, itemId))

    res = gw2api.fetch_requests(req, 2, TIMEOUT)
    print res


def test_skins():
    req = gw2api.request_skins()
    res = gw2api.fetch_requests(req, 1, TIMEOUT)
    print res


def test_skin_details():
    oneItem = [27]
    moreItems = [27, 28, 29, 30, 31]
    req = []
    req.append(gw2api.request_skin_details(oneItem))
    req.append(gw2api.request_skin_details(moreItems))
    res = gw2api.fetch_requests(req, 2, TIMEOUT)
    print res


def test_continents():
    req = []
    req.append(gw2api.request_continents())
    req.append(gw2api.request_continents(id=1))
    req.append(gw2api.request_continents(id=1, floor=1))
    req.append(gw2api.request_continents(id=1, floor=1, regions=5))
    req.append(gw2api.request_continents(id=1, floor=1, regions=5, maps=34))
    req.append(gw2api.request_continents(id=1, floor=1, regions=5, maps=34, options='sectors'))
    req.append(gw2api.request_continents(id=1, floor=1, regions=5, maps=34, options='pois'))
    req.append(gw2api.request_continents(id=1, floor=1, regions=5, maps=34, options='tasks'))

    res = gw2api.fetch_requests(req, 5, TIMEOUT)
    pprint.pprint(res)


def test_maps():
    oneMapId = [15]
    multipleMapIds = [17, 18, 19, 20]
    req = []
    req.append(gw2api.request_maps())
    req.append(gw2api.request_maps(lang='de', ids=oneMapId))
    req.append(gw2api.request_maps(ids=multipleMapIds))
    res = gw2api.fetch_requests(req, 5, TIMEOUT)
    pprint.pprint(res)


def test_listings():
    oneItem = [19684]
    multipleItems = [19684, 19709, 19970, 19971]
    req = []
    req.append(gw2api.request_listings())
    req.append(gw2api.request_listings(oneItem))
    req.append(gw2api.request_listings(multipleItems))
    res = gw2api.fetch_requests(req, 5, TIMEOUT)
    pprint.pprint(res)


def test_prices():
    oneItem = [19684]
    multipleItems = [19684, 19709, 19970, 19971]
    req = []
    req.append(gw2api.request_prices())
    req.append(gw2api.request_prices(oneItem))
    req.append(gw2api.request_prices(multipleItems))
    res = gw2api.fetch_requests(req, 5, TIMEOUT)
    pprint.pprint(res)


def test_exchange():
    exchange_type = 'coins_to_gems'
    amount = 10000
    req = []
    req.append(gw2api.request_exchange(exchange_type, amount))
    exchange_type = 'gems_to_coins'
    amount = 100
    req.append(gw2api.request_exchange(exchange_type, amount))
    res = gw2api.fetch_requests(req, 5, TIMEOUT)
    pprint.pprint(res)


def test_build():
    req = gw2api.request_build()
    res = gw2api.fetch_requests(req, 1, TIMEOUT)
    print res


def test_colors():
    oneColorId = [10]
    multipleColorIds = [11, 12, 13, 14, 15]
    req = []
    req.append(gw2api.request_colors())
    req.append(gw2api.request_colors(oneColorId))
    req.append(gw2api.request_colors(multipleColorIds))
    res = gw2api.fetch_requests(req, 5, TIMEOUT)
    pprint.pprint(res)


def test_files():
    oneFileId = ['wvw_castle']
    multipleFileIds = ["wvw_castle", "ui_supply", "map_bank", "map_guild_bank"]
    req = []
    req.append(gw2api.request_files())
    req.append(gw2api.request_files(oneFileId))
    req.append(gw2api.request_files(multipleFileIds))
    res = gw2api.fetch_requests(req, 5, TIMEOUT)
    pprint.pprint(res)


def test_quaggans():
    oneQuaggansId = ['bowl']
    multipleQuaggansIds = ["bowl", "box", "breakfast"]
    req = []
    req.append(gw2api.request_quaggans())
    req.append(gw2api.request_quaggans(oneQuaggansId))
    req.append(gw2api.request_quaggans(multipleQuaggansIds))
    res = gw2api.fetch_requests(req, 5, TIMEOUT)
    pprint.pprint(res)


def test_worlds():
    oneWorldId = [1001]
    multipleWorldIds = [1001, 1002, 1003, 1004, 1005]
    req = []
    req.append(gw2api.request_worlds())
    req.append(gw2api.request_worlds(oneWorldId))
    req.append(gw2api.request_worlds(multipleWorldIds))
    res = gw2api.fetch_requests(req, 5, TIMEOUT)
    pprint.pprint(res)

def test_matches():
    req = gw2api.request_matches()
    res = gw2api.fetch_requests(req, 1, timeout=TIMEOUT)
    pprint.pprint(res)

def test_match_details():
    match_id = '1-4'
    req = gw2api.request_match_details(match_id)
    res = gw2api.fetch_requests(req, 1, timeout=TIMEOUT)
    pprint.pprint(res)

def test_objective_names():
    req = gw2api.request_objective_names(lang='en')
    res = gw2api.fetch_requests(req, 1, timeout=TIMEOUT)
    pprint.pprint(res)

def test_event_names():
    req = gw2api.request_event_names(lang='en')
    res = gw2api.fetch_requests(req, 1, timeout=TIMEOUT)
    pprint.pprint(res)

def test_event_details():
    event_id = '5744C3F3-E1A5-40D4-8349-3DEB7FBA9EA8'
    req = []
    req.append(gw2api.request_event_details())
    req.append(gw2api.request_event_details(lang='de', event_id=event_id))
    res = gw2api.fetch_requests(req, 2, timeout=TIMEOUT)
    pprint.pprint(res)

def test_guild_details():
    guild_id = '75FD83CF-0C45-4834-BC4C-097F93A487AF'
    guild_name = 'Veterans Of Lions Arch'
    req = []
    req.append(gw2api.request_guild_details(guild_id = guild_id))
    req.append(gw2api.request_guild_details(guild_name = guild_name))
    res = gw2api.fetch_requests(req, 2, timeout=TIMEOUT)
    pprint.pprint(res)

def run_test():
    test_account()
    test_characters()
    test_character_details()
    test_transactions_current_buys()
    test_transactions_current_sells()
    test_transactions_history_buys()
    test_transactions_history_sells()
    test_tokeninfo()
    test_items()
    test_item_details()
    test_recipe()
    test_recipe_details()
    test_recipe_search()
    test_skins()
    test_skin_details()
    test_continents()
    test_maps()
    test_listings()
    test_prices()
    test_exchange()
    test_build()
    test_colors()
    test_files()
    test_quaggans()
    test_worlds()
    test_matches()
    test_match_details()
    test_objective_names()
    test_event_names()
    test_event_details()
    test_guild_details()
    print "All tests passed! :)"

if __name__ == "__main__":
    run_test()