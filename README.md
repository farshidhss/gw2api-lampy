# README #

Welcome to GW2API-Lampy for python. 

### What is this repository for? ###

*  This is a python wrapper to Guild Wars 2 API version 2. This wrapper supports parallel data access. 

Version 1.3:

* Adds all endpoints for the version 1 of the Guild Wars 2 API. Mainly WvW, events, and guild endpoints.  

Version 1.2: 

* Adds test cases for all functions in file test.py.

### How do I get set up? ###
You need concurrent package to use this library. Concurrent package is a very well known library used for multithreading in python 2.7 and 3. 

Concurrent for python 3.x: https://docs.python.org/3/library/concurrent.futures.html

Concurrent for python 2.7: https://pypi.python.org/pypi/futures

### How to use ###
Each function in the API returns a request and feeding all the requests to the method fetch_requests([req], number of parallel calls, timeout) would return a list of responses. Here is an example to retrieve all the data item ids:
```
#!python
import gw2api 
import json

def get_items_ids():
    url = gw2api.get_prices_ids()
    data = gw2api.fetch_requests([url],1,60)
    allItems = json.loads(data[0])
    return allItems
```
There are test cases written in file test.py which covers simple test cases and calls to all functions in gw2api. You can use that as an example on how to use the wrapper and to make sure all functions are working.

### Who do I talk to? ###

* Repo owner is Farshid. You can reach me via issue tracker or farshid.hassani[at]gmail.com
* Please visit my website for further news and works: [http://farshidhassani.com](http://www.farshidhassani.com)